|  Code | Description |
|-------------- | -------------- | 
| aiannhhomelands | American Indian, Alaska Native and... | 
| cnectas | combined New England city and town... | 
| congressionaldistricts | Congressional districts | 
| counties | counties in all states | 
| csas | combined statistical areas | 
| divisions | divisions | 
| elementaryschooldistricts | elementary school districts | 
| msas | metropolitian statistical areas | 
| nationwide | nationwide data | 
| nectas | New England city and town areas | 
| places | Census-designated places | 
| pumas | public use microdata areas | 
| regions | regions | 
| secondaryschooldistricts | secondary school districts | 
| statelegislativedistricts | statehouse districts | 
| states | states | 
| tracts | Census tracts | 
| unifiedschooldistricts | unified school districts | 
| urbanareas | urban areas | 
| zctas | ZIP Code tabulation areas |